﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DanielHopes2Dlib
{
    public class RenderManager
    {
        private readonly RenderTarget2D _drawBuffer;
        private readonly GraphicsDeviceManager _graphicsDeviceManager;
        private readonly int _screenWidth;
        private readonly int _screenHeight;
        private readonly Game _game;
        private readonly SpriteBatch _spriteBatch;

        public bool IsEnabled { get; set; }

        public RenderManager(Game game, GraphicsDeviceManager graphicsDeviceManager, int width, int height)
        {
            _game = game;
            _graphicsDeviceManager = graphicsDeviceManager;
            _screenWidth = width;
            _screenHeight = height;

            _spriteBatch = new SpriteBatch(graphicsDeviceManager.GraphicsDevice);

            PresentationParameters pp = graphicsDeviceManager.GraphicsDevice.PresentationParameters;

            _drawBuffer = new RenderTarget2D(graphicsDeviceManager.GraphicsDevice,
                                            width, height, false,
                                            SurfaceFormat.Color,
                                            DepthFormat.None,
                                            pp.MultiSampleCount, RenderTargetUsage.DiscardContents);

            IsEnabled = true;
        }

        public void SetFullScreen(bool fullScreen)
        {
            if (fullScreen)
            {
                GraphicsAdapter adapter = _graphicsDeviceManager.GraphicsDevice.Adapter;
                _graphicsDeviceManager.PreferredBackBufferWidth = adapter.CurrentDisplayMode.Width;
                _graphicsDeviceManager.PreferredBackBufferHeight = adapter.CurrentDisplayMode.Height;
            }
            else
            {
                _graphicsDeviceManager.PreferredBackBufferWidth = _screenWidth;
                _graphicsDeviceManager.PreferredBackBufferHeight = _screenHeight;
            }

            if (fullScreen != _graphicsDeviceManager.IsFullScreen)
                _graphicsDeviceManager.ToggleFullScreen();
        }
        public void SetRenderTarget()
        {
            if (!IsEnabled)
                return;

            _graphicsDeviceManager.GraphicsDevice.SetRenderTarget(_drawBuffer);
        }
        public void Draw()
        {
            if (!IsEnabled)
                return;

            PresentationParameters presentation = _graphicsDeviceManager.GraphicsDevice.PresentationParameters;

            float outputAspect = _game.Window.ClientBounds.Width / (float)_game.Window.ClientBounds.Height;
            float preferredAspect = _screenWidth / (float)_screenHeight;

            Rectangle dst;

            if (outputAspect <= preferredAspect)
            {
                int presentHeight = (int)((_game.Window.ClientBounds.Width / preferredAspect) + 0.5f);
                int barHeight = (_game.Window.ClientBounds.Height - presentHeight) / 2;
                dst = new Rectangle(0, barHeight, _game.Window.ClientBounds.Width, presentHeight);
            }
            else
            {
                int presentWidth = (int)((_game.Window.ClientBounds.Height * preferredAspect) + 0.5f);
                int barWidth = (_game.Window.ClientBounds.Width - presentWidth) / 2;
                dst = new Rectangle(barWidth, 0, presentWidth, _game.Window.ClientBounds.Height);
            }

            _graphicsDeviceManager.GraphicsDevice.SetRenderTarget(null);
            _graphicsDeviceManager.GraphicsDevice.Clear(ClearOptions.Target, Color.Black, 1.0f, 0);

            _spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied, SamplerState.PointClamp);
            _spriteBatch.Draw(_drawBuffer, dst, Color.White);
            _spriteBatch.End();
        }
    }
}
