﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Text;

namespace DanielHopes2Dlib
{
    public static class SoundManager
    {
        public static float Volume;
        public static float MusicVolume;
        public static float SoundEffectsVolume;
        public static Dictionary<string, SoundEffect> songs;
        public static SoundEffectInstance currentSong;
        private static State state;
        private static bool menuStarts = false;
        public static bool startRun = false;
        public static bool finishSoon = false;
        public static bool secondWas = false;
        public static bool dead = false;
        public static bool deadPlayed = false;
        public static bool ends;

        public static void Load(ContentManager content)
        {
            songs = new Dictionary<string, SoundEffect>()
            {
                {"menuStart" ,content.Load<SoundEffect>("Sounds/menuStart")},
                {"menuLoop" ,content.Load<SoundEffect>("Sounds/menuLoop")},
                {"hit" ,content.Load<SoundEffect>("Sounds/hit")},
                {"boost" ,content.Load<SoundEffect>("Sounds/boost")},
                {"dash" ,content.Load<SoundEffect>("Sounds/dash")},
                {"run" ,content.Load<SoundEffect>("Sounds/startRun")},
                {"run1" ,content.Load<SoundEffect>("Sounds/RunLoop1")},
                {"run2" ,content.Load<SoundEffect>("Sounds/RunLoop2")},
                {"end1" ,content.Load<SoundEffect>("Sounds/RunEnd")},
                {"ends" ,content.Load<SoundEffect>("Sounds/ends")}
            };
            Volume = 0.5f;
            MusicVolume = 1;
            SoundEffectsVolume = 1;
            SoundEffect.Initialize();
            MediaPlayer.Volume = MusicVolume * Volume;
            SoundEffect.MasterVolume = SoundEffectsVolume * Volume;
            MediaPlayer.IsRepeating = true;
        }
        public static void Update(State state)
        {
            SoundManager.state = state;
            MediaPlayer.Volume = MusicVolume * Volume;
            SoundEffect.MasterVolume = SoundEffectsVolume * Volume;

            if (state is null)
            {
                return;
            }
            MediaPlayer.IsRepeating = true;

            if (state is MenuState)
            {
                ends = false;
                if (!menuStarts)
                {
                    var song = songs["menuStart"].CreateInstance();
                    Console.WriteLine("Play Start menu");
                    song.IsLooped = false;
                    currentSong = song;
                    currentSong.Play();
                    menuStarts = true;
                }
                else
                {
                    if (currentSong.State == SoundState.Stopped)
                    {
                        Console.WriteLine("Play Loop menu");
                        var song = songs["menuLoop"].CreateInstance();
                        song.IsLooped = true;
                        currentSong = song;
                        currentSong.Play();
                    }
                }
            }  
            else if(state is FirstLevel)
            {
                if (menuStarts)
                {
                    menuStarts = false;
                    Console.WriteLine("Stop menu");
                    currentSong.Stop();
                }
                if (!startRun)
                {
                    var song = songs["run"].CreateInstance();
                    Console.WriteLine("Play Start Run");
                    song.IsLooped = false;
                    currentSong = song;
                    currentSong.Play();
                    startRun = true;
                }
                else
                {
                    if (dead)
                    {
                        if (!deadPlayed)
                        {
                            currentSong.Stop();
                            secondWas = false;
                            Console.WriteLine("Play run end (Dead)");
                            var song = songs["end1"].CreateInstance();
                            song.IsLooped = false;
                            currentSong = song;
                            currentSong.Play();
                            deadPlayed = true;
                        }
                    }
                    else
                    {
                        if (currentSong.State == SoundState.Stopped)
                        {
                            Console.WriteLine("Play Loop run first");
                            var song = songs["run1"].CreateInstance();
                            song.IsLooped = true;
                            currentSong = song;
                            currentSong.Play();
                        }
                        else if (finishSoon)
                        {
                            if (!secondWas)
                            {
                                currentSong.Stop();
                                secondWas = true;
                                Console.WriteLine("Play Loop run second");
                                var song = songs["run2"].CreateInstance();
                                song.IsLooped = true;
                                currentSong = song;
                                currentSong.Play();
                            }
                        }
                        else if (secondWas)
                        {
                            currentSong.Stop();
                            Console.WriteLine("Play Loop run first after second");
                            var song = songs["run1"].CreateInstance();
                            song.IsLooped = true;
                            currentSong = song;
                            secondWas = false;
                            currentSong.Play();
                        }
                    }
                }
            }
            else if(state is EndState)
            {
                if(!ends)
                {
                    ends = true;
                    var song = songs["ends"].CreateInstance();
                    song.Volume = 0.4f;
                    song.IsLooped = true;
                    currentSong.Stop();
                    currentSong = song;
                    currentSong.Play();
                }
            }
            
        }
        public static void Unload()
        {
            if (currentSong != null)
            {
                currentSong.Stop();
                currentSong.Dispose();
            }
        }
    }
}
