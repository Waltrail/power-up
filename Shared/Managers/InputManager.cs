﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace DanielHopes2Dlib
{
    public static class InputManager
    {
        public static KeyboardState currentKeyboardState = new KeyboardState();
        public static KeyboardState lastKeyboardState = new KeyboardState();
        public static GamePadState currentGamePadState = new GamePadState();
        public static GamePadState lastGamePadState = new GamePadState();

        public static bool Up => (IsKeyDown(Keys.W) || IsKeyDown(Keys.Up) || IsLeftThumbSticksUp());
        public static bool Left => (IsKeyDown(Keys.A) || IsLeftThumbSticksLeft());
        public static bool Right => (IsKeyDown(Keys.D) || IsLeftThumbSticksRight());
        public static bool Down => (IsKeyDown(Keys.S) || IsKeyDown(Keys.Down) || IsLeftThumbSticksDown());


        public static bool MenuUp => (IsNewKeyPress(Keys.W) || IsNewKeyPress(Keys.Up) || IsNewLeftThumbSticksUp());
        public static bool MenuLeft => (IsNewKeyPress(Keys.A) || IsNewKeyPress(Keys.Left) || IsNewLeftThumbSticksLeft());
        public static bool MenuRight => (IsNewKeyPress(Keys.D) || IsNewKeyPress(Keys.Right) || IsNewLeftThumbSticksRight());
        public static bool MenuDown => (IsNewKeyPress(Keys.S) || IsNewKeyPress(Keys.Down) || IsNewLeftThumbSticksDown());

        public static bool Boost => (IsNewKeyPress(Keys.Space) || IsNewButtonPress(Buttons.RightShoulder));

        public static bool ShiflLeft => (IsRightThumbSticksLeft() || IsNewKeyPress(Keys.Q) || IsNewKeyPress(Keys.Left));
        public static bool ShiflRight => (IsRightThumbSticksRight() || IsNewKeyPress(Keys.E) || IsNewKeyPress(Keys.Right));

        public static void Update()
        {
            lastKeyboardState = currentKeyboardState;
            currentKeyboardState = Keyboard.GetState();
            lastGamePadState = currentGamePadState;
            currentGamePadState = GamePad.GetState(0);
        }
        
        public static bool IsNewKeyPress(Keys key)
        {
            return (currentKeyboardState.IsKeyDown(key) &&
                lastKeyboardState.IsKeyUp(key));
        }
        public static bool IsNewButtonPress(Buttons button)
        {
            return (currentGamePadState.IsButtonDown(button) &&
                lastGamePadState.IsButtonUp(button));
        }

        public static bool IsKeyDown(Keys key)
        { return (currentKeyboardState.IsKeyDown(key)); }

        public static bool IsButtonDown(Buttons button)
        { return (currentGamePadState.IsButtonDown(button)); }

        public static bool IsNewKeyRelease(Keys key)
        {
            return (lastKeyboardState.IsKeyDown(key) &&
                currentKeyboardState.IsKeyUp(key));
        }
        public static bool IsNewButtonRelease(Buttons button)
        {
            return (lastGamePadState.IsButtonDown(button) &&
                currentGamePadState.IsButtonUp(button));
        }

        public static bool IsLeftThumbSticksLeft()
        {
            if (currentGamePadState.ThumbSticks.Left.X < -0.5f)
                return true;
            else
                return false;
        }
        public static bool IsLeftThumbSticksRight()
        {
            if (currentGamePadState.ThumbSticks.Left.X > 0.5f)
                return true;
            else
                return false;
        }
        public static bool IsLeftThumbSticksUp()
        {
            if (currentGamePadState.ThumbSticks.Left.Y > 0.5f)
                return true;
            else
                return false;
        }
        public static bool IsLeftThumbSticksDown()
        {
            if (currentGamePadState.ThumbSticks.Left.Y < -0.5f)
                return true;
            else
                return false;
        }

        public static bool IsRightThumbSticksLeft()
        {
            if (currentGamePadState.ThumbSticks.Right.X < -0.5f && lastGamePadState.ThumbSticks.Right.X > -0.5f)
                return true;
            else
                return false;
        }
        public static bool IsRightThumbSticksRight()
        {
            if (currentGamePadState.ThumbSticks.Right.X > 0.5f && lastGamePadState.ThumbSticks.Right.X < 0.5f)
                return true;
            else
                return false;
        }
        public static bool IsRightThumbSticksUp()
        {
            if (currentGamePadState.ThumbSticks.Right.Y > 0.5f)
                return true;
            else
                return false;
        }
        public static bool IsRightThumbSticksDown()
        {
            if (currentGamePadState.ThumbSticks.Right.Y < -0.5f)
                return true;
            else
                return false;
        }

        public static bool IsNewLeftThumbSticksLeft()
        {
            if (currentGamePadState.ThumbSticks.Left.X < -0.5f && lastGamePadState.ThumbSticks.Left.X > -0.5f)
                return true;
            else
                return false;
        }
        public static bool IsNewLeftThumbSticksRight()
        {
            if (currentGamePadState.ThumbSticks.Left.X > 0.5f && lastGamePadState.ThumbSticks.Left.X < 0.5f)
                return true;
            else
                return false;
        }
        public static bool IsNewLeftThumbSticksUp()
        {
            if (currentGamePadState.ThumbSticks.Left.Y > 0.5f && lastGamePadState.ThumbSticks.Left.Y < 0.5f)
                return true;
            else
                return false;
        }
        public static bool IsNewLeftThumbSticksDown()
        {
            if (currentGamePadState.ThumbSticks.Left.Y < -0.5f && lastGamePadState.ThumbSticks.Left.Y > -0.5f)
                return true;
            else
                return false;
        }
    }
}
