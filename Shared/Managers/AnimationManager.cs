﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace DanielHopes2Dlib
{
    public class AnimationManager
    {
        private float timer;

        public Animation _animation;        
        public Color Color;
        public Point Scale;
        public float CutRightWidth;
        public float CutTopHeight;

        public bool Flip { get; set; }
        public Vector2 Position { get; set; }
        

        public Rectangle destinationRectangle { get
            {
                return new Rectangle(Position.ToPoint() + new Point(0, (int)(_animation.FrameHight *  (1 - CutTopHeight))), Scale);
            }
        }

        public AnimationManager(Animation animation)
        {
            _animation = animation;
            Color = Color.White;
            Scale = new Point((int)(animation.FrameWight * CutRightWidth), (int)(animation.FrameHight * CutTopHeight));
            CutRightWidth = 1;
            CutTopHeight = 1;
        }

        public void Play(Animation animation)
        {
            Scale = new Point((int)(animation.FrameWight * CutRightWidth), (int)(animation.FrameHight * CutTopHeight));
            if (_animation == animation)
            {
                return;
            }
            _animation = animation;
            _animation.CurrentFrame = 0;
            timer = 0f;
        }
        public void Stop()
        {
            timer = 0;
            _animation.CurrentFrame = 0;
        }
        public void Update(GameTime gameTime)
        {
            timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (timer > _animation.Speed)
            {
                timer = 0f;
                _animation.CurrentFrame++;
                if (_animation.CurrentFrame >= _animation.FrameCount && _animation.IsLooping)
                {
                    _animation.CurrentFrame = 0;
                }
                if (_animation.CurrentFrame >= _animation.FrameCount && !_animation.IsLooping)
                {
                    _animation.CurrentFrame = _animation.FrameCount - 1;
                }
            }
            if (CutRightWidth < 0)
            {
                CutRightWidth = 0;
            }
            if(CutTopHeight < 0)
            {
                CutTopHeight = 0;
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            if (!Flip)
            {                
                spriteBatch.Draw(_animation.Texture,
                                 destinationRectangle,
                                 new Rectangle(_animation.CurrentFrame * _animation.FrameWight, 0, (int)(_animation.FrameWight * CutRightWidth), (int)(_animation.FrameHight * CutTopHeight)),
                                 Color);                
            }
            else
            {
                spriteBatch.Draw(_animation.Texture,
                                 destinationRectangle,
                                 new Rectangle(_animation.CurrentFrame * _animation.FrameWight, 0, (int)(_animation.FrameWight * CutRightWidth), (int)(_animation.FrameHight * CutTopHeight)),
                                 Color, 0 , Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
            }
        }
    }
}
