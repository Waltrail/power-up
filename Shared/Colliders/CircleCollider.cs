﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _2DLib.Colliders
{
    public class CircleCollider : Collider
    {
        public float Radius;
        public float Diameter { get { return Radius * 2; } }

        public CircleCollider()
        {

        }

        public CircleCollider(Vector2 Position,Vector2 PlayerPosition, float Radius)
        {
            offsetPosition = Position - PlayerPosition;
            position = Position;
            this.Radius = Radius;
        }

        public override bool IsColliding(List<Collider> colliders)
        {
            foreach (var collider in colliders)
            {
                if (collider is aabbCollider)
                {
                    float circleDistanceX = Math.Abs(position.X - ((aabbCollider)collider).Left);
                    float circleDistanceY = Math.Abs(position.Y - ((aabbCollider)collider).Top);

                    if (circleDistanceX > (((aabbCollider)collider).Right / 2 + Radius)) { return false; }
                    if (circleDistanceY > (((aabbCollider)collider).Bottom / 2 + Radius)) { return false; }

                    if (circleDistanceX <= (((aabbCollider)collider).Right / 2)) { return true; }
                    if (circleDistanceY <= (((aabbCollider)collider).Bottom / 2)) { return true; }

                    double cornerDistance_sq = Math.Pow(circleDistanceX - ((aabbCollider)collider).Right / 2, 2) +
                                               Math.Pow(circleDistanceY - ((aabbCollider)collider).Bottom / 2, 2);

                    return (cornerDistance_sq <= Math.Pow(Radius, 2));
                }
                else if (collider is CircleCollider)
                {
                    return Math.Pow(position.X - ((CircleCollider)collider).position.X, 2) + 
                           Math.Pow(position.Y - ((CircleCollider)collider).position.Y, 2) <= 
                           Math.Pow(Radius + ((CircleCollider)collider).Radius, 2);
                }
                else if (collider is PolyCollider)
                {
                    int next = 0;
                    for (int current = 0; current < ((PolyCollider)collider).Vertices.Count; current++)
                    {
                        next = current + 1;
                        if (next == ((PolyCollider)collider).Vertices.Count) next = 0;
                        Vector2 vc = ((PolyCollider)collider).Vertices[current];
                        Vector2 vn = ((PolyCollider)collider).Vertices[next];

                        bool collision = lineCircle(vc, vn, position, Radius);
                        if (collision) return true;
                    }

                    if (polygonPoint((PolyCollider)collider, position))
                        return true;

                    return false;
                }
            }
            return false;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            throw new NotImplementedException();
        }
    }
}
