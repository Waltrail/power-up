﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2DLib.Colliders
{
    public abstract class Collider
    {
        public Vector2 position;
        public Vector2 offsetPosition;
        protected Texture2D dot;
        public virtual void UpdatePosition(Vector2 Position)
        {
            position = Position + offsetPosition;
        }
        public abstract bool IsColliding(List<Collider> colliders);
        public abstract void Draw(SpriteBatch spriteBatch);

        public static bool lineCircle(Vector2 lineA, Vector2 lineB, Vector2 CirclePosition, float radius)
        {
            bool inside1 = pointCircle(lineA, CirclePosition, radius);
            bool inside2 = pointCircle(lineB, CirclePosition, radius);
            if (inside1 || inside2) return true;

            float LineLength = Vector2.Distance(lineA, lineB);

            float dot = (((CirclePosition.X - lineA.X) * (lineB.X - lineA.X)) + ((CirclePosition.Y - lineA.X) * (lineB.Y - lineA.Y))) / (float)Math.Pow(LineLength, 2);

            float closestX = lineA.X + (dot * (lineB.X - lineA.X));
            float closestY = lineA.Y + (dot * (lineB.Y - lineA.Y));

            bool onSegment = linePoint(lineA, lineB, new Vector2(closestX, closestY));
            if (!onSegment) return false;
            float distance = Vector2.Distance(CirclePosition, new Vector2(closestX, closestY));

            if (distance <= radius)
            {
                return true;
            }
            return false;
        }

        public static bool lineLine(Vector2 line1A, Vector2 line1B, Vector2 line2A, Vector2 line2B)
        {
            float uA = ((line2B.X - line2A.X) * (line1A.Y - line2A.Y) - (line2B.Y - line2A.Y) * (line1A.X - line2A.X)) / ((line2B.Y - line2A.Y) * (line1B.X - line1A.X) - (line2B.X - line2A.X) * (line1B.Y - line1A.Y));
            float uB = ((line1B.X - line1A.X) * (line1A.Y - line2A.Y) - (line1B.Y - line1A.Y) * (line1A.X - line2A.X)) / ((line2B.Y - line2A.Y) * (line1B.X - line1A.X) - (line2B.X - line2A.X) * (line1B.Y - line1A.Y));

            if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1)
            {
                return true;
            }
            return false;
        }

        public static bool polygonPoint(PolyCollider polyCollider, Vector2 point)
        {
            bool collision = false;

            int next = 0;
            for (int current = 0; current < polyCollider.Vertices.Count; current++)
            {
                next = current + 1;
                if (next == polyCollider.Vertices.Count)
                    next = 0;

                Vector2 currentVertex = polyCollider.Vertices[current] + polyCollider.position;
                Vector2 nextVertex = polyCollider.Vertices[next] + polyCollider.position;

                if (((currentVertex.Y > point.Y && nextVertex.Y < point.Y) || (currentVertex.Y < point.Y && nextVertex.Y > point.Y)) &&
                     (point.X < (nextVertex.X - currentVertex.X) * (point.Y - currentVertex.Y) / (nextVertex.Y - currentVertex.Y) + currentVertex.X))
                {
                    collision = !collision;
                }
            }
            return collision;
        }
        public static bool polygonPoint2(PolyCollider polyCollider, Vector2 point)
        {
            int[][] q_patt = { new int[] { 0, 1 }, new int[] { 3, 2 } };

            if (polyCollider.Vertices.Count < 3)
                return false;
            Vector2 pred_pt = polyCollider.Vertices[0];
            pred_pt.X += polyCollider.position.X;
            pred_pt.Y += polyCollider.position.Y;
            pred_pt.X -= point.X;
            pred_pt.Y -= point.Y;
            int y, x;
            if (pred_pt.Y < 0)
            {
                y = 1;
            }
            else
            {
                y = 0;
            }
            if (pred_pt.X < 0)
            {
                x = 1;
            }
            else
            {
                x = 0;
            }
            int pred_q = q_patt[y][x];
            float w = 0;
            for (int i = 0; i < polyCollider.Vertices.Count; i++)
            {
                Vector2 cur_pt = polyCollider.Vertices[i];
                cur_pt.X += polyCollider.position.X;
                cur_pt.Y += polyCollider.position.Y;
                cur_pt.X -= point.X;
                cur_pt.Y -= point.Y;
                if (cur_pt.Y < 0)
                {
                    y = 1;
                }
                else
                {
                    y = 0;
                }
                if (cur_pt.X < 0)
                {
                    x = 1;
                }
                else
                {
                    x = 0;
                }
                int q = q_patt[y][x];

                switch (q - pred_q)
                {
                    case -3:
                        ++w;
                        break;
                    case 3:
                        --w;
                        break;
                    case -2:
                        if (pred_pt.X * cur_pt.Y >= pred_pt.Y * cur_pt.X) ++w;
                        break;
                    case 2:
                        if (!(pred_pt.X * cur_pt.Y >= pred_pt.Y * cur_pt.X)) --w;
                        break;
                }
                pred_pt = cur_pt;
                pred_q = q;
            }
            return w != 0;

        }
        public static bool linePoint(Vector2 lineA, Vector2 lineB, Vector2 point)
        {
            float d1 = Vector2.Distance(point, lineA);
            float d2 = Vector2.Distance(point, lineB);

            float lineLen = Vector2.Distance(lineA, lineB);

            float buffer = 0.1f;

            if (d1 + d2 >= lineLen - buffer && d1 + d2 <= lineLen + buffer)
            {
                return true;
            }
            return false;
        }

        public static bool pointCircle(Vector2 Point, Vector2 CirclePosition, float radius)
        {
            float distance = Vector2.Distance(Point, CirclePosition);
            if (distance <= radius)
            {
                return true;
            }
            return false;
        }
    }
}
