﻿using System;
using System;
using System.Collections.Generic;
using System.Text;
using DanielHopes2Dlib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace _2DLib.Colliders
{
    public class aabbCollider : Collider
    {
        protected Vector2 size;

        public float Left { get { return position.X; } }
        public float Right { get { return position.X + size.X; } }
        public float Top { get { return position.Y; } }
        public float Bottom { get { return position.Y + size.Y; } }

        public Vector2 Center { get { return new Vector2(position.X + (size.X / 2), position.Y + (size.Y / 2)); } }

        public aabbCollider()
        {
            position = new Vector2();
            size = new Vector2();
            dot = new Texture2D(GameClass.graphics.GraphicsDevice, 1, 1);
            Color[] colors = { new Color(255, 255, 255) };
            dot.SetData(colors);
        }
        public aabbCollider(Vector2 Position, Vector2 PlayerPosition, Vector2 Size)
        {
            offsetPosition = Position - PlayerPosition;
            position = Position;
            size = Size;
            dot = new Texture2D(GameClass.graphics.GraphicsDevice, 1, 1);
            Color[] colors = { new Color(255, 255, 255) };
            dot.SetData(colors);
        }
        

        public override bool IsColliding(List<Collider> colliders)
        {
            foreach (var collider in colliders)
            {
                if (collider is aabbCollider)
                {
                    return Left <= ((aabbCollider)collider).Right &&
                            Right >= ((aabbCollider)collider).Left &&
                            Top <= ((aabbCollider)collider).Bottom &&
                            Bottom >= ((aabbCollider)collider).Top;
                }
                else if (collider is CircleCollider)
                {
                    float circleDistanceX = Math.Abs(((CircleCollider)collider).position.X - Left);
                    float circleDistanceY = Math.Abs(((CircleCollider)collider).position.Y - Top);

                    if (circleDistanceX > (Right / 2 + ((CircleCollider)collider).Radius)) { return false; }
                    if (circleDistanceY > (Bottom / 2 + ((CircleCollider)collider).Radius)) { return false; }

                    if (circleDistanceX <= (Right / 2)) { return true; }
                    if (circleDistanceY <= (Bottom / 2)) { return true; }

                    double cornerDistance_sq = Math.Pow(circleDistanceX - Right / 2, 2) +
                                               Math.Pow(circleDistanceY - Bottom / 2, 2);

                    return cornerDistance_sq <= Math.Pow(((CircleCollider)collider).Radius, 2);
                }
                else if (collider is PolyCollider)
                {                    
                    Vector2 current = new Vector2();
                    Vector2 next = new Vector2();
                    for (int i = 0; i < ((PolyCollider)collider).Vertices.Count; i++)
                    {
                        int nextI = i + 1;
                        if (nextI == ((PolyCollider)collider).Vertices.Count)
                            nextI = 0;

                        for (int j = 0; j < 4; j++)
                        {
                            int nextJ = j + 1;
                            if (nextJ == 4)
                                nextJ = 0;

                            switch (j)
                            {
                                case 0:
                                    current = position;
                                    break;
                                case 1:
                                    current = new Vector2(Right, Top);
                                    break;
                                case 2:
                                    current = new Vector2(Right, Bottom);
                                    break;
                                case 3:
                                    current = new Vector2(Left, Bottom);
                                    break;
                            }

                            switch (nextJ)
                            {
                                case 0:
                                    next = position;
                                    break;
                                case 1:
                                    next = new Vector2(Right, Top);
                                    break;
                                case 2:
                                    next = new Vector2(Right, Bottom);
                                    break;
                                case 3:
                                    next = new Vector2(Left, Bottom);
                                    break;
                            }

                            if (lineLine(((PolyCollider)collider).Vertices[i] + ((PolyCollider)collider).position, ((PolyCollider)collider).Vertices[nextI] + ((PolyCollider)collider).position, current, next))
                            {
                                return true;
                            }
                        }
                    }                    
                }                
            }
            return false;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(dot, new Rectangle(position.ToPoint()                 - new Point(1), new Point(2)), Color.OrangeRed);
            spriteBatch.Draw(dot, new Rectangle(new Point((int)Right,(int)Top)     - new Point(1), new Point(2)), Color.OrangeRed);
            spriteBatch.Draw(dot, new Rectangle(new Point((int)Right, (int)Bottom) - new Point(1), new Point(2)), Color.OrangeRed);
            spriteBatch.Draw(dot, new Rectangle(new Point((int)Left, (int)Bottom)  - new Point(1), new Point(2)), Color.OrangeRed);
        }
    }
}
