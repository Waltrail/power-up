﻿using System;
using System.Collections.Generic;
using System.Text;
using DanielHopes2Dlib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _2DLib.Colliders
{
    public class PolyCollider : Collider
    {
        public List<Vector2> Vertices;

        public PolyCollider()
        {
            Vertices = new List<Vector2>();
            position = new Vector2();
        }

        public PolyCollider(Vector2 Position, Vector2 PlayerPosition, List<Vector2> Vertices)
        {
            this.Vertices = Vertices;
            offsetPosition = Position - PlayerPosition;
            position = Position;

            dot = new Texture2D(GameClass.graphics.GraphicsDevice, 1, 1);
            Color[] colors = { new Color(255, 255, 255) };
            dot.SetData(colors);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            foreach (var Vertex in Vertices)
            {
                spriteBatch.Draw(dot, new Rectangle(position.ToPoint() - new Point(1) + Vertex.ToPoint(), new Point(2)), Color.Yellow);
            }
        }

        public override bool IsColliding(List<Collider> colliders)
        {
            if (Vertices.Count < 3)
            {
                return false;
            }
            foreach (var collider in colliders)
            {
                if (collider is aabbCollider)
                {
                    Vector2 current = new Vector2();
                    Vector2 next = new Vector2();
                    for (int i = 0; i < Vertices.Count; i++)
                    {
                        int nextI = i + 1;
                        if (nextI == Vertices.Count)
                            nextI = 0;

                        for (int j = 0; j < 4; j++)
                        {
                            int nextJ = j + 1;
                            if (nextJ == 4)
                                nextJ = 0;

                            switch (j)
                            {
                                case 0:
                                    current = ((aabbCollider)collider).position;
                                    break;
                                case 1:
                                    current = new Vector2(((aabbCollider)collider).Right, ((aabbCollider)collider).Top);
                                    break;
                                case 2:
                                    current = new Vector2(((aabbCollider)collider).Right, ((aabbCollider)collider).Bottom);
                                    break;
                                case 3:
                                    current = new Vector2(((aabbCollider)collider).Left, ((aabbCollider)collider).Bottom);
                                    break;
                            }

                            switch (nextJ)
                            {
                                case 0:
                                    next = ((aabbCollider)collider).position;
                                    break;
                                case 1:
                                    next = new Vector2(((aabbCollider)collider).Right, ((aabbCollider)collider).Top);
                                    break;
                                case 2:
                                    next = new Vector2(((aabbCollider)collider).Right, ((aabbCollider)collider).Bottom);
                                    break;
                                case 3:
                                    next = new Vector2(((aabbCollider)collider).Left, ((aabbCollider)collider).Bottom);
                                    break;
                            }

                            if (lineLine(Vertices[i] + position, Vertices[nextI] + position, current, next))
                            {
                                return true;
                            }
                        }
                    }
                    for (int j = 0; j < 4; j++)
                    {
                        switch (j)
                        {
                            case 0:
                                current = ((aabbCollider)collider).position;
                                break;
                            case 1:
                                current = new Vector2(((aabbCollider)collider).Right, ((aabbCollider)collider).Top);
                                break;
                            case 2:
                                current = new Vector2(((aabbCollider)collider).Right, ((aabbCollider)collider).Bottom);
                                break;
                            case 3:
                                current = new Vector2(((aabbCollider)collider).Left, ((aabbCollider)collider).Bottom);
                                break;
                        }
                        if (polygonPoint2(this, current))
                        {
                            return true;
                        }
                    }
                }
                else if (collider is CircleCollider)
                {
                    int next = 0;
                    for (int current = 0; current < Vertices.Count; current++)
                    {
                        next = current + 1;
                        if (next == Vertices.Count) next = 0;
                        Vector2 vc = Vertices[current] + position;
                        Vector2 vn = Vertices[next] + position;

                        bool collision = lineCircle(vc, vn, ((CircleCollider)collider).position, ((CircleCollider)collider).Radius);
                        if (collision) return true;
                    }

                    if (polygonPoint2(this, ((CircleCollider)collider).position))
                        return true;

                    return false;
                }
                else if (collider is PolyCollider)
                {
                    if (((PolyCollider)collider).Vertices.Count < 3)
                    {
                        return false;
                    }
                    for (int i = 0; i < Vertices.Count; i++)
                    {
                        int nextI = i + 1;
                        if (nextI == Vertices.Count)
                            nextI = 0;

                        for (int j = 0; j < ((PolyCollider)collider).Vertices.Count; j++)
                        {
                            int nextJ = j + 1;
                            if (nextJ == ((PolyCollider)collider).Vertices.Count)
                                nextJ = 0;

                            if (lineLine(Vertices[i] + position, Vertices[nextI] + position, ((PolyCollider)collider).Vertices[j], ((PolyCollider)collider).Vertices[nextJ]))
                            {
                                return true;
                            }
                        }
                        if (polygonPoint2(((PolyCollider)collider), Vertices[i]))
                        {
                            return true;
                        }
                    }
                    for (int j = 0; j < ((PolyCollider)collider).Vertices.Count; j++)
                    {
                        if (polygonPoint2(this, ((PolyCollider)collider).Vertices[j]))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
}

