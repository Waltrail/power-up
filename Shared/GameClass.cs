﻿#region Using Statements
using System;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

#endregion

namespace DanielHopes2Dlib
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameClass : Game
    {
        internal static GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private Thread SecondaryThread;
        private State _currentState;
        private State _nextState;
        private ContentManager constContent;
        private float Alfa;
        private float timer;

        internal static Random Random;
        internal static SpriteFont font;

        public static int backbufferWidth;
        public static int backbufferHeight;

        private static Color color;
        internal static Color Color
        {
            get => color;
            set
            {
                color = value;
            }
        }
        internal RenderManager renderer { get; set; }
        public static int ScreenWight { get; internal set; }
        public static int ScreenHight { get; internal set; }

        public GameClass()
        {
            ScreenWight = 1280;
            ScreenHight = 720;
            timer = 0;
            Window.AllowUserResizing = true;

            Random = new Random();
            constContent = new ContentManager(Services);
            graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = ScreenWight,
                PreferredBackBufferHeight = ScreenHight
            };

            Content.RootDirectory = "Content";
            constContent.RootDirectory = Content.RootDirectory;
        }

        protected override void Initialize()
        {
            IsMouseVisible = false;
            Alfa = 0;
            Color = Color.Black;

            renderer = new RenderManager(this, graphics, ScreenWight, ScreenHight);
            renderer.SetFullScreen(false);

            Camera2D.Init(graphics.GraphicsDevice.Viewport, new Rectangle());
            _nextState = new MenuState(this, graphics.GraphicsDevice);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            SoundManager.Load(constContent);


            SecondaryThread = new Thread(() => {
                if (_nextState != null)
                {
                    _nextState.Load(Content);
                }
            });
            SecondaryThread.SetApartmentState(ApartmentState.STA);
            SecondaryThread.Start();
            font = constContent.Load<SpriteFont>("Fonts/PixelFont");

            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void UnloadContent()
        {
            if (_currentState != null)
            {
                _currentState.UnLoad();
            }
            SoundManager.Unload();
        }

        protected override void Update(GameTime gameTime)
        {
            InputManager.Update();
            SoundManager.Update(_currentState);
            
            if (_nextState != null && !SecondaryThread.IsAlive)
            {
                UnloadContent();
                SecondaryThread = new Thread(() => {
                    if (_nextState != null)
                    {
                        _nextState.Load(Content);
                    }
                });
                SecondaryThread.SetApartmentState(ApartmentState.STA);
                SecondaryThread.Start();
            }
            if (_nextState != null && _nextState.IsLoaded)
            {
                _currentState = _nextState;
                _nextState = null;
            }
            if (SecondaryThread.IsAlive)
            {
                if (Alfa < 1)
                {
                    Alfa += (float)gameTime.ElapsedGameTime.TotalSeconds / 2;
                }
            }
            else
            {
                if (Alfa > 0)
                {
                    Alfa -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.F4) || InputManager.IsNewButtonPress(Buttons.Start))
            {
                if(!graphics.IsFullScreen)
                    renderer.SetFullScreen(true);
                else
                {
                    renderer.SetFullScreen(false);
                    Window.Position = new Point(0, 20);
                }
            }

            if (_currentState != null)
                _currentState.Update(gameTime);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            renderer.SetRenderTarget();

            GraphicsDevice.Clear(Color);
            if (_currentState != null)
                _currentState.Draw(gameTime, spriteBatch);

            if (Alfa > 0.1f)
            {
                spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied, SamplerState.PointClamp, null, null, null, Matrix.CreateScale(2));
                Texture2D pixel = new Texture2D(graphics.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
                pixel.SetData(new[] { Color.White });
                spriteBatch.Draw(pixel, new Rectangle(0, 0, ScreenWight, ScreenHight), Color.White * Alfa);
                spriteBatch.End();
            }
            renderer.Draw();
        }

        public void ChangeState(State state)
        {
            _nextState = state;
        }

    }
}
