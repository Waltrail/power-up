﻿using DanielHopes2Dlib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DLib.UI
{
    public class BoostBar : Sprite
    {
        protected Texture2D bottomBar;
        public float Boost;
        public BoostBar(Texture2D texture) : base(texture)
        {

        }

        public BoostBar(Texture2D texture, Dictionary<string, Animation> _animations, Texture2D Bot) : base(texture)
        {
            animations = _animations;
            animationManager = new AnimationManager(animations.First().Value);
            bottomBar = Bot;
            Boost = 100;
        }

        public BoostBar(Dictionary<string, Animation> _animations) : base(_animations)
        {
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (bottomBar != null)
            {
                spriteBatch.Draw(bottomBar, position, color);
            }
            if (animationManager != null)
            {
                animationManager.Draw(spriteBatch);
            }
            if (_texture != null)
            {
                spriteBatch.Draw(_texture, position, color);
            }
            else
            {
                throw new Exception("There's no animations or sprites");
            }
        }

        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            foreach (var sprite in sprites)
            {
                if (sprite is Player)
                {
                    Boost = ((Player)sprite).BoostLeft;
                }
            }
            if (animations != null)
            {
                animationManager.Position = position;
                animationManager.CutTopHeight = Boost / 100;
                SetAnimation();
                animationManager.Update(gameTime);
                if(Boost < 10)
                {
                    animationManager.Color = Color.LightGray;
                }
                else
                    animationManager.Color = Color.White;
            }
            foreach (var collider in colliders)
            {
                collider.UpdatePosition(Position);
            }
        }
    }
}
