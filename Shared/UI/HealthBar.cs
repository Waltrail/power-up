﻿using DanielHopes2Dlib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DLib.UI
{
    public class HealthBar : Sprite
    {
        protected Texture2D bottomBar;
        public float Health;
        public HealthBar(Texture2D texture) : base(texture)
        {

        }

        public HealthBar(Texture2D texture, Dictionary<string, Animation> _animations, Texture2D Bot) : base(texture)
        {
            animations = _animations;
            animationManager = new AnimationManager(animations.First().Value);
            bottomBar = Bot;
            Health = 100;
        }

        public HealthBar(Dictionary<string, Animation> _animations) : base(_animations)
        {
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if(bottomBar != null)
            {
                spriteBatch.Draw(bottomBar, position, color);
            }
            if (animationManager != null)
            {
                animationManager.Draw(spriteBatch);
            }
            if (_texture != null)
            {
                spriteBatch.Draw(_texture, position, color);
            }
            else
            {
                throw new Exception("There's no animations or sprites");
            }
        }

        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            foreach(var sprite in sprites)
            {
                if(sprite is Player)
                {
                    Health = ((Player)sprite).health;
                }
            }
            if (animations != null)
            {
                animationManager.Position = position;
                animationManager.CutRightWidth = Health / 100;
                SetAnimation();
                animationManager.Update(gameTime);
            }
            foreach (var collider in colliders)
            {
                collider.UpdatePosition(Position);
            }
        }
    }
}
