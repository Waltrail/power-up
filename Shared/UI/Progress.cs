﻿using DanielHopes2Dlib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2DLib.UI
{
    public class Progress : Sprite
    {
        public float progress;
        private Vector2 dotPos;
        private Vector2 PlayerPos;
        private Texture2D player;
        private Texture2D background;
        public float Lenght;

        public Progress(Texture2D texture) : base(texture)
        {
        }
        public Progress(Texture2D texture, Texture2D Player) : base(texture)
        {
            dotPos = position;
            progress = 0;
            Lenght = 80;
            player = Player;
            PlayerPos = new Vector2(0, Lenght * (1-(progress / 100))) + position;
            background = new Texture2D(GameClass.graphics.GraphicsDevice, 20, (int)Lenght);
            Color[] colors = new Color[20 * (int)Lenght];
        }
        public Progress(Dictionary<string, Animation> _animations) : base(_animations)
        {
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (_texture != null)
            {
                spriteBatch.Draw(_texture, position, color);
            }
            if(player != null)
            {
                spriteBatch.Draw(player, PlayerPos, color);
            }
        }

        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            base.Update(gameTime, sprites);
            foreach(var sp in sprites)
            {
                if(sp is Player)
                {
                    progress = ((Player)sp).progress;
                }
            }
            if (progress < 0)
                progress = 0;
            if (progress > 80)
                progress = 80;
            PlayerPos = new Vector2(0, Lenght * (1 - (progress / 80))) + position;
            if(progress > 60)
            {
                SoundManager.finishSoon = true;
            }
            else
            {
                SoundManager.finishSoon = false;
            }
        }
    }
}
