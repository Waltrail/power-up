﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DanielHopes2Dlib
{
    public class EndState : State
    {
        public EndState(GameClass game, GraphicsDevice graphicsDevice) : base(game, graphicsDevice)
        {
            index = 1;
        }

        private float timer;
        private Sprite logos;
        private float index;
        private Sprite cantions;
        private bool showCaptions;
        private float position;

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (!IsLoaded)
            {
                return;
            }
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, Camera2D.Transform);
            if(!showCaptions)
                logos.Draw(gameTime, spriteBatch);
            if (showCaptions)
            {
                cantions.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();
        }

        public override void Load(ContentManager content)
        {
            _content = content;
            var logosimg = _content.Load<Texture2D>("Sprites/ends");

            var logosani = new Dictionary<string, Animation>()
            {
                {"ends", new Animation(logosimg, 5) { Speed = 2f, IsLooping = false} }
            };
            position = 236;
            cantions = new Sprite(_content.Load<Texture2D>("Sprites/captions"));
            logos = new Sprite(logosani);
            Camera2D.CameraPosition = new Vector2(logos.Position.X + (logos.Rectangle.Width / 2), 0);
            IsLoaded = true;
            showCaptions = false;
            Camera2D.Zoom = 6;
        }

        public override void Update(GameTime gameTime)
        {
            if (!IsLoaded)
            {
                return;
            }
            GameClass.Color = new Color(7,7,8);
            logos.Update(gameTime, null);
            timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            Console.WriteLine(timer);
            
            Camera2D.CameraPosition = new Vector2(logos.Position.X + (logos.Rectangle.Width / 2), 0);
            if (timer > 11)
            {
                showCaptions = true;
                Camera2D.Zoom = 1f;
                position += (float)gameTime.ElapsedGameTime.TotalMilliseconds / 20;
                if (position > 1800)
                {
                    _game.Exit();
                }
            }
            if (InputManager.IsNewKeyPress(Keys.Escape) || InputManager.IsNewButtonPress(Buttons.Back))
            {
                _game.ChangeState(new MenuState(_game, _graphicsDevice));
            }            
            Camera2D.Follow(logos);
            if (showCaptions)
            {
                Camera2D.Follow(new Vector2(cantions.Rectangle.Width / 4 + 56, position));
            }
            Camera2D.Update(gameTime);
        }
        public override void UnLoad()
        {

        }

    }
}
