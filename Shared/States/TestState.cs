﻿using _2DLib.Colliders;
using _2DLib.Models;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace DanielHopes2Dlib
{
    public class TestState : State
    {        
        public List<Sprite> mainlayer;
        public MouseState state;
        public Box Box;
        public TestState(GameClass game, GraphicsDevice graphicsDevice) : base(game, graphicsDevice)
        {
            _game = game;
            _graphicsDevice = graphicsDevice;
            var boxTex = new Texture2D(graphicsDevice, 1, 1);
            Color[] colors = { new Color(255, 255, 255)};
            boxTex.SetData(colors);
            Box = new Box(boxTex);

            IsLoaded = false;

            Camera2D.DefaultVelosity = 1f;
            Camera2D.VelosityBoost = 1f;
            Camera2D.VelosityBound = 1f;
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (!IsLoaded)
            {
                return;
            }

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, Camera2D.Transform);            
            foreach (var s in mainlayer)
            {
                s.Draw(gameTime, spriteBatch);
            }           
            spriteBatch.End();
        }

        public override void Load(ContentManager content)
        {
            _content = content;
            Camera2D.CameraPosition = new Vector2(0, -500);

            var leaf1Tex = _content.Load<Texture2D>("Sprites/level/leaf1");
            var leaf2Tex = _content.Load<Texture2D>("Sprites/level/leaf2");
            var leaf3Tex = _content.Load<Texture2D>("Sprites/level/leaf3");
            var leaf1Collider = new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(31,0),
                    new Vector2(39,0),
                    new Vector2(46,3),
                    new Vector2(46,12),
                    new Vector2(4,22),
                    new Vector2(0,22)
                });
            var leaf2Collider = new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(0,8),
                    new Vector2(11,0),
                    new Vector2(18,0),
                    new Vector2(53,22),
                    new Vector2(42,27),
                    new Vector2(15,27),
                    new Vector2(0,14)
                });
            var leaf3Collider = new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(0,13),
                    new Vector2(11,0),
                    new Vector2(15,0),
                    new Vector2(24,12),
                    new Vector2(13,54),
                    new Vector2(0,27),
                });


            var leaf1 = new Leaf(leaf1Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(31,0),
                    new Vector2(39,0),
                    new Vector2(46,3),
                    new Vector2(46,12),
                    new Vector2(4,22),
                    new Vector2(0,22)
                })) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-500, 700)) };
            var leaf2 = new Leaf(leaf1Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(31,0),
                    new Vector2(39,0),
                    new Vector2(46,3),
                    new Vector2(46,12),
                    new Vector2(4,22),
                    new Vector2(0,22)
                })) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-500, 700)) };
            var leaf3 = new Leaf(leaf1Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(31,0),
                    new Vector2(39,0),
                    new Vector2(46,3),
                    new Vector2(46,12),
                    new Vector2(4,22),
                    new Vector2(0,22)
                })) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-500, 700)) };
            var leaf12 = new Leaf(leaf1Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(31,0),
                    new Vector2(39,0),
                    new Vector2(46,3),
                    new Vector2(46,12),
                    new Vector2(4,22),
                    new Vector2(0,22)
                })) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-500, 700)) };


            var leaf4 = new Leaf(leaf2Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(0,8),
                    new Vector2(11,0),
                    new Vector2(18,0),
                    new Vector2(53,22),
                    new Vector2(42,27),
                    new Vector2(15,27),
                    new Vector2(0,14)
                })) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-500, 700)) };
            var leaf5 = new Leaf(leaf2Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(0,8),
                    new Vector2(11,0),
                    new Vector2(18,0),
                    new Vector2(53,22),
                    new Vector2(42,27),
                    new Vector2(15,27),
                    new Vector2(0,14)
                })) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-500, 700)) };
            var leaf6 = new Leaf(leaf2Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(0,8),
                    new Vector2(11,0),
                    new Vector2(18,0),
                    new Vector2(53,22),
                    new Vector2(42,27),
                    new Vector2(15,27),
                    new Vector2(0,14)
                })) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-500, 700)) };
            var leaf7 = new Leaf(leaf2Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(0,8),
                    new Vector2(11,0),
                    new Vector2(18,0),
                    new Vector2(53,22),
                    new Vector2(42,27),
                    new Vector2(15,27),
                    new Vector2(0,14)
                })) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-500, 700)) };


            var leaf8 = new Leaf(leaf3Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(0,13),
                    new Vector2(11,0),
                    new Vector2(15,0),
                    new Vector2(24,12),
                    new Vector2(13,54),
                    new Vector2(0,27),
                })) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-500, 700)) };
            var leaf9 = new Leaf(leaf3Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(0,13),
                    new Vector2(11,0),
                    new Vector2(15,0),
                    new Vector2(24,12),
                    new Vector2(13,54),
                    new Vector2(0,27),
                })) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-500, 700)) };
            var leaf10 = new Leaf(leaf3Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(0,13),
                    new Vector2(11,0),
                    new Vector2(15,0),
                    new Vector2(24,12),
                    new Vector2(13,54),
                    new Vector2(0,27),
                })) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-500, 700)) };
            var leaf11 = new Leaf(leaf3Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(0,13),
                    new Vector2(11,0),
                    new Vector2(15,0),
                    new Vector2(24,12),
                    new Vector2(13,54),
                    new Vector2(0,27),
                })) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-500, 700)) };

            var boostAnim = new Dictionary<string, Animation>()
            {
                {"", new Animation(_content.Load<Texture2D>("Sprites/level/boost"), 2){ Speed = 0.3f } }
            };
            var healthTex = _content.Load<Texture2D>("Sprites/level/health");

            var boost = new Boost(boostAnim);

            mainlayer = new List<Sprite>()
            {
                leaf1,
                leaf2,
                leaf3,
                leaf4,
                leaf5,
                leaf6,
                leaf7,
                leaf8,
                leaf9,
                leaf10,
                leaf11,
                leaf12,
                boost,
                Box
            };

            Camera2D.Zoom = 2.8f;
            IsLoaded = true;

            GameClass.Color = new Color(10, 5, 15);
        }

        public override void UnLoad()
        {

        }

        public override void Update(GameTime gameTime)
        {
            if (!IsLoaded)
            {
                return;
            }

            state = Mouse.GetState();
            foreach (var s in mainlayer)
            {
                s.Update(gameTime, mainlayer);
            }
            Camera2D.Follow(state.Position.ToVector2() + new Vector2(10,10));
            Camera2D.Update(gameTime);
        }
    }
}
