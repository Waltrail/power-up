﻿using DanielHopes2Dlib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace DanielHopes2Dlib
{
    public class MenuState : State
    {
        private List<Button> _components;
        private List<Sprite> _sprites;
        private Sprite Options;
        private bool ShowOptions;

        public MenuState(GameClass game, GraphicsDevice graphicsDevice) : base(game, graphicsDevice)
        {
            Camera2D.DefaultVelosity = 1f;
            Camera2D.VelosityBoost = 1f;
            Camera2D.VelosityBound = 1f;
            Camera2D.CameraPosition = new Vector2();
            Camera2D.Zoom = 3f;
            Camera2D.CameraPosition = new Vector2(0, -500);
            IsLoaded = false;
        }

        private void OptionsButtonClick(object sender, EventArgs e)
        {
            ShowOptions = true;
        }

        private void QuitButtonClick(object sender, EventArgs e)
        {
            _game.Exit();
        }

        private void NewGameButtonClick(object sender, EventArgs e)
        {
            _game.ChangeState(new FirstLevel(_game, _graphicsDevice));
        }


        public override void Update(GameTime gameTime)
        {

            if (!ShowOptions)
            {
                if (InputManager.MenuUp)
                {
                    for (int i = 0; i < _components.Count; i++)
                    {
                        if (_components[i].isHover)
                        {
                            _components[i].isHover = false;
                            if (i == 0)
                            {
                                _components[2].isHover = true;
                            }
                            else
                            {
                                _components[i - 1].isHover = true;
                            }
                            break;
                        }
                    }
                }
                if (InputManager.MenuDown)
                {
                    for (int i = 0; i < _components.Count; i++)
                    {
                        if (_components[i].isHover)
                        {
                            _components[i].isHover = false;
                            if (i == 2)
                            {
                                _components[0].isHover = true;
                            }
                            else
                            {
                                _components[i + 1].isHover = true;
                            }
                            break;
                        }
                    }
                }
                foreach (var sprite in _sprites)
                {
                    sprite.Update(gameTime, _sprites);
                }
                foreach (var component in _components)
                {
                    component.Update(gameTime);
                }
            }
            if (ShowOptions && (InputManager.IsNewKeyPress(Keys.Escape) || InputManager.IsNewButtonPress(Buttons.Back)))
            {
                ShowOptions = false;
                SoundManager.currentSong.Resume();
            }
            Camera2D.Follow(new Vector2(0, -370));
            Camera2D.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, Camera2D.Transform);
            foreach (var s in _sprites)
            {
                s.Draw(gameTime, spriteBatch);
            }
            foreach(var button in _components)
            {
                button.Draw(gameTime, spriteBatch);
            }
            if (ShowOptions)
            {
                Options.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();
        }

        public override void Load(ContentManager content)
        {
            _content = content;
            var PlayTex = _content.Load<Texture2D>("Sprites/menu/play");
            var OptionsTex = _content.Load<Texture2D>("Sprites/menu/controls");
            var QuitTex = _content.Load<Texture2D>("Sprites/menu/quit");
            var CharTex = _content.Load<Texture2D>("Sprites/menu/char");
            var BackTex = _content.Load<Texture2D>("Sprites/menu/back");

            var Back = new Sprite(BackTex) { Position = new Vector2(-200, -465) };
            var Char = new Sprite(CharTex) { Position = new Vector2(0, -408) };

            var PlayButton = new Button(PlayTex,"")
            {
                position = new Vector2(-192, -378)
            };
            var OptionsButton = new Button(OptionsTex, "")
            {
                position = new Vector2(-192, -341)
            };
            var QuitButton = new Button(QuitTex, "")
            {
                position = new Vector2(-192, -304)
            };
            _components = new List<Button>()
            {
                PlayButton,
                OptionsButton,
                QuitButton,
            };
            PlayButton.Click += NewGameButtonClick;
            QuitButton.Click += QuitButtonClick;
            OptionsButton.Click += OptionsButtonClick;
            PlayButton.isHover = true;
            ShowOptions = false;
            
            var options = content.Load<Texture2D>("Sprites/menu/options");
            Options = new Sprite(options) { Position = new Vector2(-200, -465) };
            
            _sprites = new List<Sprite>()
            {
                Back,
                Char
            };
            Camera2D.Zoom = 3f;
            IsLoaded = true;
            GameClass.Color = new Color(218, 67, 40);
        }

        public override void UnLoad()
        {

        }
    }
}
