﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DanielHopes2Dlib
{
    public class GameState : State
    {
        public Player player;
        public Vector2 playerStartPos;
        public List<Sprite> backlayer;
        public List<Sprite> mainlayer;
        public List<Sprite> frontlayer;
        public List<Sprite> parallaxlayer;
        public List<Sprite> parallaxlayer2;
        public List<Sprite> parallaxlayer3;
        public List<Sprite> parallaxlayer4;
        public List<Sprite> parallaxlayer5;
        public List<Sprite> clouds;
        public List<Sprite> UIsLayer;
        protected Vector2 parallaxSpeed;

        protected bool pause;
        protected Sprite pauseBack;
        protected Sprite pauseDead;
        protected List<Button> buttons;

        protected List<Sprite> main;
        public List<Sprite> Leaves;
        public List<Sprite> Heals;
        public List<Sprite> Boostes;
        protected int LeavesCount;
        protected int BoostersCount;
        protected int HealsCount;

        public GameState(GameClass game, GraphicsDevice graphicsDevice) : base(game, graphicsDevice)
        {
            _game = game;
            _graphicsDevice = graphicsDevice;
            playerStartPos = new Vector2(-50,-200);
            IsLoaded = false;

            Camera2D.DefaultVelosity = 1f;
            Camera2D.VelosityBoost = 1f;
            Camera2D.VelosityBound = 1f;
        }
        public override void Load(ContentManager content)
        {            
            _content = content;

            Camera2D.CameraPosition = new Vector2(0, -500);
            var PlayerUp = new Animation(_content.Load<Texture2D>("Sprites/player/flying"), 5) { IsLooping = true, Speed = 0.2f };
            var PlayerSlide = new Animation(_content.Load<Texture2D>("Sprites/player/slide"), 5) { IsLooping = true, Speed = 0.2f };
            var PlayerHit = new Animation(_content.Load<Texture2D>("Sprites/player/hit"), 2) { IsLooping = true, Speed = 0.1f };

            var PlayerAnimations = new Dictionary<string, Animation>()
            {
                { "air" ,PlayerUp },
                { "slide", PlayerSlide },
                { "hit" , PlayerHit }
            };
            player = new Player(PlayerAnimations) { Position = playerStartPos, BoxFix = 0 };

            parallaxlayer = new List<Sprite>();
            backlayer = new List<Sprite>();
            mainlayer = new List<Sprite>();            
            frontlayer = new List<Sprite>();
        }

        public override void Update(GameTime gameTime)
        {
            worldPosition = Camera2D.CameraPosition;
            if (!IsLoaded)
            {
                return;
            }

            if (player.progress > 80)
            {
                _game.ChangeState(new EndState(_game, _graphicsDevice));
                return;
            }

            if (InputManager.IsNewKeyPress(Microsoft.Xna.Framework.Input.Keys.Escape) || InputManager.IsNewButtonPress(Microsoft.Xna.Framework.Input.Buttons.Back))
            {
                if (pause)
                {
                    pause = false;
                }
                else
                    pause = true;
            }
            if (pause)
            {
                if (InputManager.MenuUp)
                {
                    for (int i = 0; i < buttons.Count; i++)
                    {
                        if (buttons[i].isHover)
                        {
                            buttons[i].isHover = false;
                            if (i == 0)
                            {
                                buttons[2].isHover = true;
                            }
                            else
                            {
                                buttons[i - 1].isHover = true;
                            }
                            break;
                        }
                    }
                }
                if (InputManager.MenuDown)
                {
                    for (int i = 0; i < buttons.Count; i++)
                    {
                        if (buttons[i].isHover)
                        {
                            buttons[i].isHover = false;
                            if (i == 2)
                            {
                                buttons[0].isHover = true;
                            }
                            else
                            {
                                buttons[i + 1].isHover = true;
                            }
                            break;
                        }
                    }
                }
                pauseDead.Update(gameTime, null);
                pauseBack.Update(gameTime, null);
                foreach (var button in buttons)
                {
                    button.Update(gameTime);
                }
                return;
            }

            foreach (var s in backlayer)
            {
                s.Update(gameTime, backlayer);
            }

            foreach (var s in parallaxlayer)
            {
                s.Update(gameTime, parallaxlayer);
            }
            if(LeavesCount < 0)
            {
                LeavesCount = 0;
            }
            if (BoostersCount < 0)
            {
                BoostersCount = 0;
            }
            if (HealsCount < 0)
            {
                HealsCount = 0;
            }
            main = new List<Sprite>(mainlayer);
            main.AddRange(Leaves.GetRange(0,LeavesCount));
            main.AddRange(Boostes.GetRange(0, BoostersCount));
            main.AddRange(Heals.GetRange(0, HealsCount));
            foreach (var s in main)
            {
                s.Update(gameTime, main);
            }            
            foreach (var s in frontlayer)
            {
                s.Update(gameTime, frontlayer);
            }

            foreach (var s in UIsLayer)
            {
                s.Update(gameTime, mainlayer);
            }
            foreach (var s in clouds)
            {
                if (player.Position.Y > -15000)
                {
                    if (s.Position.Y > player.Position.Y * 6 + 1000)
                    {
                        s.Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next((int)player.Position.Y * 6 - 2000, (int)player.Position.Y * 6 - 1000));
                    }
                }
                s.Update(gameTime, null);
            }
            Camera2D.Follow(player);
            Camera2D.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (!IsLoaded)
            {
                return;
            }
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, Camera2D.GetViewMatrix(new Vector2(1.1f)));
            foreach (var s in parallaxlayer)
            {
                s.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, Camera2D.GetViewMatrix(new Vector2(1.2f,1.3f)));
            foreach (var s in parallaxlayer2)
            {
                s.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, Camera2D.GetViewMatrix(new Vector2(1.3f,1.5f)));
            foreach (var s in parallaxlayer3)
            {
                s.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, Camera2D.GetViewMatrix(new Vector2(1.5f,1.9f)));
            foreach (var s in parallaxlayer3)
            {
                s.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, Camera2D.GetViewMatrix(new Vector2(1.6f,1.95f)));
            foreach (var s in parallaxlayer4)
            {
                s.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, Camera2D.GetViewMatrix(new Vector2(2,3)));
            foreach (var s in parallaxlayer5)
            {
                s.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, Camera2D.GetViewMatrix(new Vector2(2, 6)));
            foreach (var s in clouds)
            {
                s.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();


            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, Camera2D.Transform);
            foreach (var s in backlayer)
            {
                s.Draw(gameTime, spriteBatch);
            }
            foreach (var s in main)
            {
                s.Draw(gameTime, spriteBatch);
            }
            foreach (var s in frontlayer)
            {
                s.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, Matrix.CreateScale(Camera2D.Zoom));
            foreach (var s in UIsLayer)
            {
                s.Draw(gameTime, spriteBatch);
            }
            if (pause)
            {
                int start = 0;
                if (player.dead)
                {
                    pauseDead.Draw(gameTime, spriteBatch);
                    start = 1;
                }
                else
                    pauseBack.Draw(gameTime, spriteBatch);
                for(int i = start; i < buttons.Count; i++)
                {
                    buttons[i].Draw(gameTime,spriteBatch);
                }
            }
            spriteBatch.End();
        }

        public override void UnLoad()
        {
            
        }
    }
}
