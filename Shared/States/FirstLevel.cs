﻿using System;
using System.Collections.Generic;
using System.Text;
using _2DLib.Colliders;
using _2DLib.Models;
using _2DLib.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DanielHopes2Dlib
{
    public class FirstLevel : GameState
    {

        public FirstLevel(GameClass game, GraphicsDevice graphicsDevice) : base(game, graphicsDevice)
        {
            parallaxSpeed = new Vector2(1.5f, 4f);
            LeavesCount = 0;
            BoostersCount = 0;
            HealsCount = 0;
            pause = false;
        }

        public override void Load(ContentManager content)
        {
            base.Load(content);

            var treesTex = _content.Load<Texture2D>("Sprites/level/trees");
            var rocksTex = _content.Load<Texture2D>("Sprites/level/rocks");
            var ground1Tex = _content.Load<Texture2D>("Sprites/level/ground1");
            var ground2Tex = _content.Load<Texture2D>("Sprites/level/ground2");
            var ground3Tex = _content.Load<Texture2D>("Sprites/level/ground3");
            var leaf1Tex = _content.Load<Texture2D>("Sprites/level/leaf1");
            var leaf2Tex = _content.Load<Texture2D>("Sprites/level/leaf2");
            var leaf3Tex = _content.Load<Texture2D>("Sprites/level/leaf3");
            var leaf1 = new Leaf(leaf1Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(31,0),
                    new Vector2(39,0),
                    new Vector2(46,3),
                    new Vector2(46,12),
                    new Vector2(4,22),
                    new Vector2(0,22)
                }))
            { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-700, -250)) };
            var leaf2 = new Leaf(leaf1Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(31,0),
                    new Vector2(39,0),
                    new Vector2(46,3),
                    new Vector2(46,12),
                    new Vector2(4,22),
                    new Vector2(0,22)
                }))
            { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-700, -250)) };
            var leaf3 = new Leaf(leaf1Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(31,0),
                    new Vector2(39,0),
                    new Vector2(46,3),
                    new Vector2(46,12),
                    new Vector2(4,22),
                    new Vector2(0,22)
                }))
            { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-700, -250)) };
            var leaf12 = new Leaf(leaf1Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(31,0),
                    new Vector2(39,0),
                    new Vector2(46,3),
                    new Vector2(46,12),
                    new Vector2(4,22),
                    new Vector2(0,22)
                }))
            { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-700, -250)) };
            var leaf13 = new Leaf(leaf1Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(31,0),
                    new Vector2(39,0),
                    new Vector2(46,3),
                    new Vector2(46,12),
                    new Vector2(4,22),
                    new Vector2(0,22)
                }))
            { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-700, -250)) };
            var leaf14 = new Leaf(leaf1Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(31,0),
                    new Vector2(39,0),
                    new Vector2(46,3),
                    new Vector2(46,12),
                    new Vector2(4,22),
                    new Vector2(0,22)
                }))
            { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-700, -250)) };


            var leaf4 = new Leaf(leaf2Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(0,8),
                    new Vector2(11,0),
                    new Vector2(18,0),
                    new Vector2(53,22),
                    new Vector2(42,27),
                    new Vector2(15,27),
                    new Vector2(0,14)
                }))
            { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-700, -250)) };
            var leaf5 = new Leaf(leaf2Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(0,8),
                    new Vector2(11,0),
                    new Vector2(18,0),
                    new Vector2(53,22),
                    new Vector2(42,27),
                    new Vector2(15,27),
                    new Vector2(0,14)
                }))
            { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-700, -250)) };
            var leaf6 = new Leaf(leaf2Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(0,8),
                    new Vector2(11,0),
                    new Vector2(18,0),
                    new Vector2(53,22),
                    new Vector2(42,27),
                    new Vector2(15,27),
                    new Vector2(0,14)
                }))
            { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-700, -250)) };
            var leaf7 = new Leaf(leaf2Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(0,8),
                    new Vector2(11,0),
                    new Vector2(18,0),
                    new Vector2(53,22),
                    new Vector2(42,27),
                    new Vector2(15,27),
                    new Vector2(0,14)
                }))
            { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-700, -250)) };


            var leaf8 = new Leaf(leaf3Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(0,13),
                    new Vector2(11,0),
                    new Vector2(15,0),
                    new Vector2(24,12),
                    new Vector2(13,54),
                    new Vector2(0,27),
                }))
            { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-700, -250)) };
            var leaf9 = new Leaf(leaf3Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(0,13),
                    new Vector2(11,0),
                    new Vector2(15,0),
                    new Vector2(24,12),
                    new Vector2(13,54),
                    new Vector2(0,27),
                }))
            { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-700, -250)) };
            var leaf10 = new Leaf(leaf3Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(0,13),
                    new Vector2(11,0),
                    new Vector2(15,0),
                    new Vector2(24,12),
                    new Vector2(13,54),
                    new Vector2(0,27),
                }))
            { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-700, -250)) };
            var leaf11 = new Leaf(leaf3Tex, new PolyCollider(new Vector2(), new Vector2(),
                new List<Vector2>() {
                    new Vector2(0,13),
                    new Vector2(11,0),
                    new Vector2(15,0),
                    new Vector2(24,12),
                    new Vector2(13,54),
                    new Vector2(0,27),
                }))
            { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-700, -250)) };


            var plantAnim = new Dictionary<string, Animation>()
            {
                { "", new Animation(_content.Load<Texture2D>("Sprites/level/plant"),2){ Speed = 0.8f, IsLooping = true } }
            };

            var trees = new Sprite(treesTex) { Position = new Vector2(-treesTex.Width / 2, -180) };
            var rocks = new Sprite(rocksTex) { Position = new Vector2(-rocksTex.Width / 2, -300) };
            var ground1 = new Sprite(ground1Tex) { Position = new Vector2(-ground1Tex.Width / 2, -130) };
            var ground2 = new Sprite(ground2Tex) { Position = new Vector2(-ground2Tex.Width / 2, -200) };
            var ground3 = new Sprite(ground3Tex) { Position = new Vector2(-ground3Tex.Width / 2, -250) };
            var Plant1 = new Sprite(plantAnim) { Position = new Vector2(-100, 0) };
            var Plant2 = new Sprite(plantAnim) { Position = new Vector2(-100, -200) };
            var Plant3 = new Sprite(plantAnim) { Position = new Vector2(-100, -400) };

            var healthbarTopTex = _content.Load<Texture2D>("Sprites/level/HealthBarTop");
            var healthbarMidTex = new Animation(_content.Load<Texture2D>("Sprites/level/HealthBarMid"), 4) { Speed = 0.05f };
            var healthbarBotTex = _content.Load<Texture2D>("Sprites/level/HealthBarBot");

            var healthbar = new HealthBar(healthbarTopTex, new Dictionary<string, Animation>() { { "", healthbarMidTex } }, healthbarBotTex) { Position = new Vector2(10, 10) };

            var BoostbarTopTex = _content.Load<Texture2D>("Sprites/level/BoostBarTop");
            var BoostbarMidTex = new Animation(_content.Load<Texture2D>("Sprites/level/BoostBarMid"), 5) { Speed = 0.05f };
            var BoostbarBotTex = _content.Load<Texture2D>("Sprites/level/BoostBarBot");

            var boostbar = new BoostBar(BoostbarTopTex, new Dictionary<string, Animation>() { { "", BoostbarMidTex } }, BoostbarBotTex) { Position = new Vector2(405, 80) };

            var progressBar = new Progress(_content.Load<Texture2D>("Sprites/level/dot"), _content.Load<Texture2D>("Sprites/level/player")) { Position = new Vector2(10, 82), };

            var boostAnim = new Dictionary<string, Animation>()
            {
                {"", new Animation(_content.Load<Texture2D>("Sprites/level/boost"), 2){ Speed = 0.3f } }
            };
            var healthTex = _content.Load<Texture2D>("Sprites/level/health");

            var boost = new Boost(boostAnim);
            var boost1 = new Boost(boostAnim);
            var boost2 = new Boost(boostAnim);

            var health = new Health(healthTex);
            var health1 = new Health(healthTex);

            pauseBack = new Sprite(_content.Load<Texture2D>("Sprites/menu/pause")) { Position = new Vector2(130, 30) };
            pauseDead = new Sprite(_content.Load<Texture2D>("Sprites/menu/pauseD")) { Position = new Vector2(130, 30) };

            var continueButton = new Button(_content.Load<Texture2D>("Sprites/menu/continue"), "") { position = new Vector2(138, 100) };
            var RestartButton = new Button(_content.Load<Texture2D>("Sprites/menu/restart"), "") { position = new Vector2(138, 100 + 34) };
            var QuitButton = new Button(_content.Load<Texture2D>("Sprites/menu/quit"), "") { position = new Vector2(138, 100 + 68) };

            var cloud1 = new Sprite(_content.Load<Texture2D>("Sprites/level/cloud1")) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-3700, -2250)) };
            var cloud2 = new Sprite(_content.Load<Texture2D>("Sprites/level/cloud2")) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-3700, -2250)) };
            var cloud3 = new Sprite(_content.Load<Texture2D>("Sprites/level/cloud3")) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-3700, -2250)) };
            var cloud4 = new Sprite(_content.Load<Texture2D>("Sprites/level/cloud4")) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-3700, -2250)) };
            var cloud5 = new Sprite(_content.Load<Texture2D>("Sprites/level/cloud5")) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-3700, -2250)) };

            var cloud11 = new Sprite(_content.Load<Texture2D>("Sprites/level/cloud1")) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-4700, -2250)) };
            var cloud21 = new Sprite(_content.Load<Texture2D>("Sprites/level/cloud2")) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-4700, -2250)) };
            var cloud31 = new Sprite(_content.Load<Texture2D>("Sprites/level/cloud3")) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-4700, -2250)) };
            var cloud41 = new Sprite(_content.Load<Texture2D>("Sprites/level/cloud4")) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-4700, -2250)) };
            var cloud51 = new Sprite(_content.Load<Texture2D>("Sprites/level/cloud5")) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-4700, -2250)) };

            var cloud12 = new Sprite(_content.Load<Texture2D>("Sprites/level/cloud1")) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-5700, -2250)) };
            var cloud22 = new Sprite(_content.Load<Texture2D>("Sprites/level/cloud2")) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-5700, -2250)) };
            var cloud32 = new Sprite(_content.Load<Texture2D>("Sprites/level/cloud3")) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-5700, -2250)) };
            var cloud42 = new Sprite(_content.Load<Texture2D>("Sprites/level/cloud4")) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-5700, -2250)) };
            var cloud52 = new Sprite(_content.Load<Texture2D>("Sprites/level/cloud5")) { Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-5700, -2250)) };

            continueButton.Click += continueButtonClick;
            RestartButton.Click += RestartButtonClick;
            QuitButton.Click += QuitButtonClick;

            continueButton.isHover = true;

            buttons = new List<Button>()
            {
                continueButton,
                RestartButton,
                QuitButton,
            };

            Leaves = new List<Sprite>()
            {
                leaf1,
                leaf2,
                leaf3,
                leaf4,
                leaf5,
                leaf6,
                leaf7,
                leaf8,
                leaf9,
                leaf10,
                leaf11,
                leaf12,
            };
            Boostes = new List<Sprite>()
            {
                boost,
                boost1,
                boost2,
                boost,
                boost1,
                boost2
            };
            Heals = new List<Sprite>()
            {
                health,
                health1,
                health,
                health1,
                health,
                health1
            };

            UIsLayer = new List<Sprite>()
            {
                healthbar,
                boostbar,
                progressBar
            };

            clouds = new List<Sprite>()
            {
                cloud1,
                cloud2,
                cloud3,
                cloud4,
                cloud5,
                cloud11,
                cloud12,
                cloud21,
                cloud22,
                cloud31,
                cloud32,
                cloud41,
                cloud42,
                cloud51,
                cloud52
            };

            parallaxlayer = new List<Sprite>()
            {
                rocks,
            };
            parallaxlayer2 = new List<Sprite>()
            {
                ground3
            };
            parallaxlayer3 = new List<Sprite>()
            {
                ground2
            };
            parallaxlayer4 = new List<Sprite>()
            {
                trees
            };
            parallaxlayer5 = new List<Sprite>()
            {
                ground1
            };

            backlayer = new List<Sprite>()
            {
                Plant1,
                Plant2,
                Plant3
            };

            mainlayer = new List<Sprite>()
            {
                player
            };
            Camera2D.Zoom = 3f;
            IsLoaded = true;

            GameClass.Color = new Color(95, 205, 228);
        }

        private void QuitButtonClick(object sender, EventArgs e)
        {
            _game.ChangeState(new MenuState(_game, _graphicsDevice));
        }

        private void RestartButtonClick(object sender, EventArgs e)
        {
            Restart();
        }

        private void continueButtonClick(object sender, EventArgs e)
        {
            pause = false;
            SoundManager.currentSong.Resume();
        }

        public override void Update(GameTime gameTime)
        {            
            base.Update(gameTime);
            

            float r = -player.Position.Y / 10000,
                  g= -player.Position.Y / 10000,
                  b = -player.Position.Y / 10000;
            

            if(r < 1)
                r = 1;
            if (g < 1)
                g = 1;
            if (b < 1)
                b = 1;

            if (r > 255)
                r = 225;
            if (g > 255)
                g = 225;
            if (b > 255)
                b = 225;

            r = 95 / r;
            g = 205 / g;
            b = 228 / b;

            var color = new Color((int)r, (int)g, (int)b);

            GameClass.Color = color;
            if (pause)
            {
                SoundManager.currentSong.Pause();
                return;
            }
            if (player.dead)
            {                
                pause = true;
                buttons[0].isHover = false;
                buttons[1].isHover = true;
            }
            else
            {
                buttons[0].isHover = false;
                buttons[1].isHover = true;
            }
            foreach (var plant in backlayer)
            {
                if (plant.Position.Y > player.Position.Y + 200)
                {
                    plant.Position = new Vector2(plant.Position.X, plant.Position.Y - 600);
                    player.progress++;
                }
                else if (plant.Position.Y < player.Position.Y - 300)
                {
                    plant.Position = new Vector2(plant.Position.X, plant.Position.Y + 600);
                    player.progress--;
                }
                plant.Position = new Vector2(plant.Position.X, plant.Position.Y - 0.08f * (float)gameTime.ElapsedGameTime.TotalMilliseconds);
            }
            foreach (var item in main)
            {
                if (item is Leaf || item is Health || item is Boost)
                {
                    if (item.Position.Y > player.Position.Y + 300 || item.Position.X > 294 || item.Position.X < -310 || item.Position.Y < player.Position.Y - 1500)
                    {
                        float x, y;
                        if (item is Leaf)
                        {
                            x = GameClass.Random.Next(-4, 5);
                            y = GameClass.Random.Next(12, 18);
                        }
                        else
                        {
                            x = GameClass.Random.Next(-2, 3);
                            y = GameClass.Random.Next(4, 10);
                        }
                        x /= 10;
                        y /= 100;
                        item.Velocity = new Vector2(x, y);
                        item.Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-900 + (int)player.Position.Y, -400 + (int)player.Position.Y));
                    }
                }
            }
            LeavesCount = (int)-player.Position.Y / 1000;
            if (LeavesCount >= Leaves.Count)
                LeavesCount = Leaves.Count;
            BoostersCount = (int)-player.Position.Y / 1500;
            if (BoostersCount >= Boostes.Count)
                BoostersCount = Boostes.Count;
            HealsCount = (int)-player.Position.Y / 2000;
            if (HealsCount >= Heals.Count)
                HealsCount = Heals.Count;

        }

        public void Restart()
        {
            player.Position = playerStartPos;
            player.dead = false;
            player.health = 100;
            player.BoostLeft = 0;
            player.UpSpeed = 0.07f;
            pause = false;
            SoundManager.startRun = false;
            SoundManager.finishSoon = false;
            SoundManager.secondWas = false;
            SoundManager.dead = false;
            SoundManager.deadPlayed = false;
        }
    }
}
