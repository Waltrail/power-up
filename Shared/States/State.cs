﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DanielHopes2Dlib
{
    public abstract class State
    {
        protected GraphicsDevice _graphicsDevice;
        protected GameClass _game;
        protected ContentManager _content;

        public bool IsLoaded;
        public float slide;
        public int elementsLoaded;
        public int elements;

        public static float cameraZoom;
        public static Vector2 worldPosition;

        public abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch);
        public abstract void Update(GameTime gameTime);
        public abstract void Load(ContentManager content);
        public abstract void UnLoad();

        public State(GameClass game, GraphicsDevice graphicsDevice)
        {
            _game = game; _graphicsDevice = graphicsDevice;
            elementsLoaded = 0;
            elements = 0;
        }
    }
}
