﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using System;

namespace DanielHopes2Dlib
{
    public class Button
    {

        public bool isHover;
        public string text = "";
        public event EventHandler Click;
        public Vector2 padding;

        private Texture2D _texture;
        private SpriteFont _font;

        public Vector2 position { get; set; }
        public Color color { get; set; }
        public Rectangle rectangle
        {
            get
            {
                return new Rectangle((int)position.X, (int)position.Y, _texture.Width, _texture.Height);
            }
        }

        public Button(Texture2D texture,string text)
        {
            _texture = texture;
            color = Color.Gray;            
            isHover = false;            
            this.text = text;
            _font = GameClass.font;
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            var _color = Color.Chocolate;
            if (isHover)
            {
                _color = Color.Wheat;
            }
            spriteBatch.Draw(_texture, rectangle, _color);
            spriteBatch.DrawString(_font, text, position + padding, _color,0 , Vector2.Zero, 0.7f,SpriteEffects.None, 0);
        }        

        public void Update(GameTime gameTime)
        {            
            if (isHover)
            {
                if (InputManager.IsNewKeyPress(Keys.Space) || InputManager.IsNewKeyPress(Keys.Enter) || InputManager.IsNewButtonPress(Buttons.X))
                {
                    Click?.Invoke(this, new EventArgs());
                }
            }
        }        
    }
}
