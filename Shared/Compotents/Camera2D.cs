﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace DanielHopes2Dlib
{
    public static class Camera2D
    {        
        private static Matrix Rotation;
        private static Vector2 centre;
        private static Vector2 PrefPosition;
        private static Vector2 Pilot;
        private static Vector2 offsetShake;
        private static Viewport viewport;        
        private static float zoom = 4.0f;
        private static float shake;
        private static float shakeTime;
        private static float shakeRadius;
        private static float shakeDecrease;

        public static Matrix scaleMatrix;
        public static Matrix OffsetView;
        public static Rectangle SceneBox;
        public static Vector2 CameraPosition;
        public static Vector2 OffsetScene;
        public static float Velosity;
        public static float DefaultVelosity;
        public static float VelosityBoost;
        public static float VelosityBound;        
        public static float OffsetY;
        public static float OffsetX;
        public static float rotation = 0;

        public static Matrix Transform { get; private set; }
        public static float Zoom
        {
            get { return zoom; }
            set
            {
                zoom = value;
                if (zoom < 0.1f)
                    zoom = 0.1f;
            }
        }

        public static void Init(Viewport newViewport, Rectangle SceneBox)
        {
            OffsetY = 0;
            OffsetX = 0;
            shake = 0;
            shakeDecrease = 0.25f;
            shakeTime = 0;
            DefaultVelosity = 0.05f;
            VelosityBoost = 0.01f;
            VelosityBound = 0.5f;
            viewport = newViewport;
            Velosity = DefaultVelosity;
            Pilot = new Vector2();
            Camera2D.SceneBox = SceneBox;
            CameraPosition = new Vector2();
            centre = new Vector2();
            OffsetScene = new Vector2(385, 345);
        }
        public static void Follow(Sprite target)
        {
            PrefPosition = centre;
            centre = new Vector2(target.Position.X + (target.Rectangle.Width / 2) + OffsetX, target.Position.Y + (target.Rectangle.Height / 2) - OffsetY);

            Pilot = Vector2.Lerp(CameraPosition, centre, Velosity);

            if (SceneBox.Width != 0 && SceneBox.Height != 0)
            {
                Pilot.X = MathHelper.Clamp(Pilot.X, SceneBox.Left + OffsetScene.X, SceneBox.Right - OffsetScene.Y);
                Pilot.Y = MathHelper.Clamp(Pilot.Y, SceneBox.Top, SceneBox.Bottom);
            }
            Rotation = Matrix.CreateRotationZ(rotation);
            var Position = Matrix.CreateTranslation(new Vector3(-Pilot.X + offsetShake.X, -Pilot.Y - offsetShake.Y, 0));

            scaleMatrix = Matrix.CreateScale(new Vector3(Zoom, Zoom, 0));

            OffsetView = Matrix.CreateTranslation(new Vector3(viewport.Width / 2 + offsetShake.X, viewport.Height / 2 - offsetShake.Y, 0));

            Transform = Position * Rotation * scaleMatrix * OffsetView;

            CameraPosition = Pilot;
        }
        public static Matrix GetViewMatrix(Vector2 parallax)
        {
            return Matrix.CreateTranslation(new Vector3(-CameraPosition * parallax, 0.0f)) *
                Rotation *
                scaleMatrix *
                Matrix.CreateTranslation(new Vector3(viewport.Width / 2 + offsetShake.X / parallax.X, viewport.Height / 2 - offsetShake.Y / parallax.Y, 0.0f));
        }
        public static void Follow(Vector2 target)
        {
            PrefPosition = centre;
            centre = new Vector2(target.X + OffsetX, target.Y - OffsetY);

            Pilot = Vector2.Lerp(CameraPosition, centre, Velosity);

            if (SceneBox.Width != 0 && SceneBox.Height != 0)
            {
                Pilot.X = MathHelper.Clamp(Pilot.X, SceneBox.Left + OffsetScene.X + offsetShake.X, SceneBox.Right - OffsetScene.Y);
                Pilot.Y = MathHelper.Clamp(Pilot.Y, SceneBox.Top, SceneBox.Bottom);
            }
            Rotation = Matrix.CreateRotationZ(rotation);
            var Position = Matrix.CreateTranslation(new Vector3(-Pilot.X, -Pilot.Y, 0));

            scaleMatrix = Matrix.CreateScale(new Vector3(Zoom, Zoom, 0));

            OffsetView = Matrix.CreateTranslation(new Vector3(viewport.Width / 2 + offsetShake.X, viewport.Height / 2 - offsetShake.Y, 0));

            Transform = Position * Rotation * scaleMatrix * OffsetView;

            CameraPosition = Pilot;
        }
        public static void Update(GameTime gameTime)
        {
            if (PrefPosition.X != centre.X)
            {
                if (Velosity <= VelosityBound)
                {
                    Velosity += VelosityBoost;
                }
            }
            else
            {
                Velosity = DefaultVelosity;
            }
            if (shakeTime > 0 && shakeRadius >= 0)
            {
                offsetShake = new Vector2((float)(Math.Sin(shake) * shakeRadius), (float)(Math.Cos(shake) * shakeRadius));
                shakeRadius -= shakeDecrease;
                shake += (150 + GameClass.Random.Next(60));
                shakeTime -= (float)gameTime.ElapsedGameTime.TotalSeconds;               
            }
            else
            {
                offsetShake = new Vector2();
            }
        }
        public static void Shake(float amount, float time)
        {
            shake = 1;
            shakeDecrease = 0.25f;
            if(shakeRadius < amount)
                shakeRadius = amount;
            shakeTime = time;
        }
        public static void Shake(float amount, float time, float decrease)
        {
            shake = 1;
            shakeDecrease = decrease;
            if (shakeRadius < amount)
                shakeRadius = amount;
            shakeTime = time;
        }
    }
}
