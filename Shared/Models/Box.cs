﻿using _2DLib.Colliders;
using DanielHopes2Dlib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DLib.Models
{
    public class Box : Sprite
    {
        public Box(Texture2D texture) : base(texture)
        {
            colliders = new List<Collider>()
            {
                new aabbCollider(position, position, new Vector2(Rectangle.Width,Rectangle.Height))
            };
        }

        public Box(Dictionary<string, Animation> _animations) : base(_animations)
        {
            colliders = new List<Collider>()
            {
                new aabbCollider(position, position, new Vector2(Rectangle.Width,Rectangle.Height))
            };
        }

        public override Rectangle Rectangle
        {
            get
            {
                if (_texture != null)
                {
                    return new Rectangle((int)position.X, (int)position.Y, 20, 20);
                }
                if (animations != null)
                {
                    if (animationManager._animation == null)
                    {
                        return new Rectangle((int)position.X + BoxFix, (int)position.Y, animations.First().Value.Texture.Width / animations.First().Value.FrameCount - (BoxFix * 2), animations.First().Value.Texture.Height);
                    }
                    else
                    {
                        return new Rectangle((int)position.X + BoxFix, (int)position.Y, animationManager._animation.Texture.Width / animationManager._animation.FrameCount - (BoxFix * 2), animationManager._animation.Texture.Height);
                    }
                }
                else
                {
                    throw new Exception("There's no animations or sprites");
                }
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (_texture != null)
            {
                spriteBatch.Draw(_texture, Rectangle, null, color);
            }
            else if (animationManager != null)
            {
                animationManager.Draw(spriteBatch);
            }
            else
            {
                throw new Exception("There's no animations or sprites");
            }
            colliders.First().Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            color = new Color(255, 255, 255);
            position = Mouse.GetState().Position.ToVector2() + new Vector2(10, 10);
            colliders.First().UpdatePosition(Position);
            foreach (var sprite in sprites)
            {
                if (sprite == this)
                {
                    continue;
                }
                if (colliders.First().IsColliding(sprite.colliders))
                {
                    color = new Color(255, 30, 40);
                }
            }
            if (animations != null)
            {
                SetAnimation();
                animationManager.Update(gameTime);
                animationManager.Position = position;
            }
        }
    }
}
