﻿using _2DLib.Colliders;
using DanielHopes2Dlib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DanielHopes2Dlib
{
    public class Health : Sprite
    {
        public Health(Texture2D texture) : base(texture)
        {
            colliders.Add(new aabbCollider(new Vector2(), new Vector2(), new Vector2(texture.Width, texture.Height)));
            float x = GameClass.Random.Next(-2, 3);
            float y = GameClass.Random.Next(12, 18);
            x /= 10;
            y /= 100;
            Velocity = new Vector2(x, y);
            colliders.First().UpdatePosition(position);
        }

        public Health(Dictionary<string, Animation> _animations) : base(_animations)
        {
            colliders.Add(new aabbCollider(new Vector2(), new Vector2(), new Vector2(_animations.First().Value.FrameWight, _animations.First().Value.FrameHight)));
            float x = GameClass.Random.Next(-2, 3);
            float y = GameClass.Random.Next(5, 10);
            x /= 10;
            y /= 100;
            Velocity = new Vector2(x, y);
            colliders.First().UpdatePosition(position);
        }

        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            position += Velocity * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            colliders.First().UpdatePosition(position);
            base.Update(gameTime, sprites);
        }
    }
}
