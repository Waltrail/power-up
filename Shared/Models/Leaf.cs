﻿using _2DLib.Colliders;
using DanielHopes2Dlib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DLib.Models
{
    public class Leaf : Sprite
    {
        
        public Leaf(Texture2D texture) : base(texture)
        {
        }
        public Leaf(Texture2D texture, PolyCollider collider) : base(texture)
        {
            colliders.Add(collider);
            colliders.First().UpdatePosition(position);
            float x = GameClass.Random.Next(-4, 5);
            float y = GameClass.Random.Next(12, 18);
            x /= 10;
            y /= 100;
            Velocity = new Vector2(x, y);            
        }

        public Leaf(Dictionary<string, Animation> _animations) : base(_animations)
        {
        }

        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            position += Velocity * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            base.Update(gameTime, sprites);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);
            //colliders.First().Draw(spriteBatch);
        }
    }
}
