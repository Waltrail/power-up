﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using _2DLib.Colliders;
using _2DLib.Models;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DanielHopes2Dlib
{
    public class Player : Sprite
    {
        public float Speed;
        private float timer = 0;
        public float UpSpeed;
        public float progress;
        public float health;
        public float BoostLeft;
        private float boostTimer;
        private float healthTimer;
        public bool isControllable;
        public bool dead;
        public Vector2 ScreenPosition;        

        public Player(Texture2D texture) : base(texture)
        {
            BoostLeft = 0;
            Speed = 0.24f;
            UpSpeed = 0.07f;
            BoxFix = 8;
            health = 100;
            healthTimer = 0;
            progress = 62;
            isControllable = true;
            dead = false;
            ScreenPosition = new Vector2();
            colliders = new List<Collider>()
            {
                new aabbCollider(position + ScreenPosition,position + ScreenPosition, new Vector2(Rectangle.Width,Rectangle.Height))
            };
            Velocity = new Vector2();
        }

        public Player(Dictionary<string, Animation> _animations) : base(_animations)
        {
            BoostLeft = 0;
            Speed = 0.24f;
            UpSpeed = 0.07f;
            BoxFix = 8;
            health = 100;
            healthTimer = 0;
            progress = 0;
            isControllable = true;
            dead = false;
            ScreenPosition = new Vector2();
            colliders = new List<Collider>()
            {
                new aabbCollider(position + ScreenPosition,position + ScreenPosition, new Vector2(Rectangle.Width,Rectangle.Height))
            };
            Velocity = new Vector2();
        }

        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            BoxFix = 8;
            Move();
            SoundManager.dead = dead;
            if (timer > 0)
                timer -= (float)gameTime.ElapsedGameTime.Milliseconds;
            else
            {
                isControllable = true;
                if (UpSpeed > 0.07f)
                    UpSpeed /= 1.02f;
            }
            if (!dead)
            {
                if (health < 100 && isControllable)
                {
                    if (healthTimer >= 2000)
                    {
                        health += 1;
                        healthTimer = 0;
                    }
                    healthTimer += gameTime.ElapsedGameTime.Milliseconds;
                }
                if (BoostLeft < 100)
                {
                    if (boostTimer >= 3000)
                    {
                        BoostLeft += 1;
                        boostTimer = 0;
                    }
                    boostTimer += gameTime.ElapsedGameTime.Milliseconds;
                }
            }
            ScreenPosition += Velocity * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (ScreenPosition.X > 194 || ScreenPosition.X < -210)
            {
                Velocity.X = 0;
            }
            if (ScreenPosition.Y > 100 || ScreenPosition.Y < -90)
            {
                Velocity.Y = 0;
            }
            if (health <= 0)
            {
                UpSpeed = -0.2f;
                ScreenPosition.Y += 0.36f * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                isControllable = false;
                dead = true;                
            }
            else
            {
                ScreenPosition.X = MathHelper.Clamp(ScreenPosition.X, -210, 194);
                ScreenPosition.Y = MathHelper.Clamp(ScreenPosition.Y, -90, 100);
            }
            position += new Vector2(0, -UpSpeed) * (float)gameTime.ElapsedGameTime.TotalMilliseconds;           
            
            colliders.First().UpdatePosition(Position + ScreenPosition);

            foreach (var sprite in sprites)
            {
                if (sprite == this)
                {
                    continue;
                }
                if (colliders.First().IsColliding(sprite.colliders))
                {
                    if (sprite is Health)
                    {
                        sprite.Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-900 + (int)Position.Y, -400 + (int)Position.Y));
                        health += 20;
                        if(health > 100)
                        {
                            health = 100;
                        }
                    }
                    if (sprite is Boost)
                    {
                        sprite.Position = new Vector2(GameClass.Random.Next(-210, 194), GameClass.Random.Next(-900 + (int)Position.Y, -400 + (int)Position.Y));
                        BoostLeft += 20;
                        if(BoostLeft > 100)
                        {
                            BoostLeft = 100;
                        }
                    }
                    if (sprite is Leaf)
                    {
                        if (isControllable)
                        {
                            health -= 20;
                            Camera2D.Shake(8, 2, 0.25f);
                            var hit = SoundManager.songs["hit"].CreateInstance();
                            hit.Volume = 0.1f;
                            hit.IsLooped = false;
                            hit.Play();
                        }
                        timer = 500;
                        isControllable = false;
                    }
                }
            }
            SetAnimation();
            if (animations != null)
            {
                animationManager.Update(gameTime);
                animationManager.Position = position + ScreenPosition;
            }
        }        

        protected override void SetAnimation()
        {
            if (animations != null)
            {
                if (!isControllable)
                {
                    animationManager.Play(animations[key: "hit"]);
                }
                else if (Velocity.X > 0)
                {
                    animationManager.Flip = false;
                    animationManager.Play(animations[key: "slide"]);
                }
                else if (Velocity.X < 0)
                {
                    animationManager.Flip = true;
                    animationManager.Play(animations[key: "slide"]);
                }                
                else if (Velocity.X == 0)
                {     
                    animationManager.Play(animations[key: "air"]);
                }
            }
        }
        private void Move()
        {
            if (isControllable)
            {
                if (InputManager.Left)
                {
                    if (Velocity.X > -Speed)
                        Velocity.X += -Speed / 20;
                }
                else if (InputManager.Right)
                {
                    if (Velocity.X < Speed)
                        Velocity.X += Speed / 20;
                }
                else
                {
                    if (Velocity.X > 0.01)
                        Velocity.X += -Speed / 60;

                    else if (Velocity.X < -0.01)
                        Velocity.X += Speed / 60;

                    else
                        Velocity.X = 0;
                }
                if (InputManager.Up)
                {
                    if (Velocity.Y > -Speed)
                        Velocity.Y += -Speed / 20;
                }
                else if (InputManager.Down)
                {
                    if (Velocity.Y < Speed)
                        Velocity.Y += Speed / 20;
                }
                else
                {
                    if (Velocity.Y > 0.01)
                        Velocity.Y += -Speed / 60;

                    else if (Velocity.Y < -0.01)
                        Velocity.Y += Speed / 60;

                    else
                        Velocity.Y = 0;
                }
                if (InputManager.Boost)
                {
                    if (BoostLeft >= 10)
                    {

                        if (UpSpeed < 1.2f)
                        {
                            UpSpeed *= 1.65f;
                            BoostLeft -= 10;
                            timer = 3000;
                            Camera2D.Shake(UpSpeed / 100, 3, 0.0001f);
                            var boost = SoundManager.songs["boost"].CreateInstance();
                            boost.Volume = 0.4f;
                            boost.IsLooped = false;
                            boost.Play();
                        }
                    }
                }
                if (InputManager.ShiflLeft)
                {
                    Velocity.X = -Speed * 1.8f;
                    Camera2D.Shake(1.5f, 1, 0.2f);
                    var dash = SoundManager.songs["dash"].CreateInstance();
                    dash.Volume = 0.5f;
                    dash.IsLooped = false;
                    dash.Play();
                }
                else if (InputManager.ShiflRight)
                {
                    Velocity.X = Speed * 1.8f;
                    Camera2D.Shake(1.5f, 1, 0.2f);
                    var dash = SoundManager.songs["dash"].CreateInstance();
                    dash.Volume = 0.5f;
                    dash.IsLooped = false;
                    dash.Play();
                }
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (_texture != null)
            {
                spriteBatch.Draw(_texture, position + ScreenPosition, color);
            }
            else if (animationManager != null)
            {
                animationManager.Draw(spriteBatch);
            }
            else
            {
                throw new Exception("There's no animations or sprites");
            }
        }
    }
}
